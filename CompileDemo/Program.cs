﻿using System;
using System.Threading.Tasks;
using CompileDemo.InteractionArea;
using CompileDemo.Services;
using CompileDemo.Services.FileObserverArea;
using CompileDemo.Services.ProcessorArea;
using CompileDemo.SettingsArea;

namespace CompileDemo
{
  class Program
  {
    private static Action currentAction;

    static void Main(string[] args)
    {
      Prepare();

      var mainTask = new Task(() =>
      {
        currentAction = StartMenu;

        while (true)
        {
          try
          {
            currentAction();
          }
          catch (Exception ex)
          {
            FaultState(ex.Message);
          }
        }
      });

      mainTask.Start();
      mainTask.Wait();
    }

    private static void Prepare()
    {
      MainDirectoryService.CleanDllDirectory();
      MainDirectoryService.CleanTemporaryDirectory();
    }

    private static void StartMenu()
    {
      var action = Interaction.Menu<StartMenu.Variants>();

      switch (action)
      {
        case InteractionArea.StartMenu.Variants.Exit:
          {
            currentAction = Exit;
            break;
          }
        case InteractionArea.StartMenu.Variants.CreateDirectories:
          {
            MainDirectoryService.Create();
            break;
          }
        case InteractionArea.StartMenu.Variants.CleanDirectories:
          {
            MainDirectoryService.CleanAll();
            break;
          }
        case InteractionArea.StartMenu.Variants.Next:
          {
            currentAction = DataGenerateAdjust;
            break;
          }
      }
    }

    private static void DataGenerateAdjust()
    {
      var dataGenerateConfig = Interaction.Input<InputDataGenerate>();

      DataGenerateSettings.Setup(dataGenerateConfig);

      currentAction = StartServices;
    }

    private static void StartServices()
    {
      StatisticSearching.Start();
      Compilator.Start();
      DllLoader.Start();
      Processor.Start();
      FileObserver.Start();
      DataGenerator.Start();

      currentAction = WorkingProcess;
    }

    private static void StopServices()
    {
      FileObserver.Stop();
      DataGenerator.Stop();
      Processor.Stop();
      DllLoader.Stop();
      Compilator.Stop();
      StatisticSearching.Stop();
    }

    private static void WorkingProcess()
    {
      Console.CancelKeyPress += (sender, e) =>
      {
        StopServices();
        currentAction = Exit;
      };

      WorkingInterfaceRefresh();
      Task.Delay(1000).Wait();
    }

    private static void WorkingInterfaceRefresh()
    {
      Interaction.Clear();
      Interaction.WriteMessage(StatisticSearching.Search());
      Interaction.WriteMessage("Нажмите Ctrl+C для завершения приложения");
    }

    private static void FaultState(string errMessage)
    {
      Interaction.WriteException(errMessage);
      Interaction.WriteMessage("Нажмите любую клавишу для выхода");
      Console.ReadKey(true);

      currentAction = Exit;
    }

    private static void Exit()
    {
      Environment.Exit(0);
    }
  }
}
