﻿using CompileDemo.InteractionArea;

namespace CompileDemo.SettingsArea
{
  internal static class DataGenerateSettings
  {
    public static void Setup(InputDataGenerate dataGenerate)
    {
      FilesPerSecond = dataGenerate.FilesPerSecond;
      ArrayLength = dataGenerate.ArrayLength;
    }

    public static uint FilesPerSecond { get; private set; }

    public static uint ArrayLength { get; private set; }
  }
}
