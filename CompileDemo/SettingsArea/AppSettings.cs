﻿using System.Collections.Generic;
using System.Configuration;

namespace CompileDemo.SettingsArea
{
  internal static class AppSettings
  {
    static AppSettings()
    {
      var appSettings = ConfigurationManager.AppSettings;

      PreCompiledScriptDir = appSettings["PreCompiledScriptDir"];
      DllDir = appSettings["ScriptCompiledDir"];

      PreProcessedDataDir = appSettings["DataPreProcessedDir"];
      ProcessedDataDir = appSettings["DataProcessedDir"];

      TemporaryDir = appSettings["TemporaryDir"];

      ScriptClassName = appSettings["ScriptClassName"];
      ScriptMethodName = appSettings["ScriptMethodName"];
    }

    public static readonly string PreCompiledScriptDir;
    public static readonly string DllDir;

    public static readonly string PreProcessedDataDir;
    public static readonly string ProcessedDataDir;

    public static readonly string TemporaryDir;

    public static readonly string ScriptClassName;
    public static readonly string ScriptMethodName;

    public const string DLL_FILE_EXTENSION = "dll";
    public const string SCRIPT_FILE_EXTENSION = "cs";
    public const string DATA_FILE_EXTENSION = "txt";
    public static readonly byte[] TEST_BYTE_ARRAY = { 1, 2 };

    public static IEnumerable<string> Directories()
    {
      yield return PreCompiledScriptDir;
      yield return DllDir;

      yield return PreProcessedDataDir;
      yield return ProcessedDataDir;

      yield return TemporaryDir;
    }
  }
}
