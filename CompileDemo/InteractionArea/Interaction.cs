﻿using System;
using System.Linq;
using CompileDemo.InteractionArea.Attributes;

namespace CompileDemo.InteractionArea
{
  internal static class Interaction
  {
    static Interaction()
    {
      Console.BackgroundColor = ConsoleColor.White;
      Console.Clear();
    }

    public static void WriteException(string exceptionMessage)
    {
      Console.ForegroundColor = ConsoleColor.Red;
      Console.WriteLine(exceptionMessage);
    }

    public static void WriteMessage(string message)
    {
      Console.ForegroundColor = ConsoleColor.Black;
      Console.WriteLine(message);
    }

    public static void WriteSuccessMessage(string message)
    {
      Console.ForegroundColor = ConsoleColor.Green;
      Console.WriteLine(message);
    }

    public static void TraceMessageWrite(string message)
    {
      Console.ForegroundColor = ConsoleColor.Gray;
      Console.WriteLine(message);
    }

    public static T Input<T>() where T : class, new()
    {
      Console.ForegroundColor = ConsoleColor.Blue;

      var inputService = new InputAttributesService<T>();

      var headerText = inputService.HeaderRead();

      Console.WriteLine(headerText);

      var questions = inputService.QuestionsRead().ToList();

      foreach (var question in questions)
      {
        string answer;
        bool validate;
        do
        {
          Console.WriteLine(question.Question);
          answer = Console.ReadLine();
          validate = inputService.AnswerTypeValidate(question.FieldName, answer);
        } while (!validate);

        question.Answer = answer;

      }

      var result = inputService.BuildResult(questions);

      return result;
    }

    public static T Menu<T>()
    {
      Console.ForegroundColor = ConsoleColor.Blue;

      var choiceVariants = Enum.GetValues(typeof(T)).Cast<int>().ToList();
      var attributeReader = new MenuAttributesReader<T>();
      var menuText = attributeReader.Read();

      ConsoleKeyInfo keyInput;
      bool choiceMenuItem;

      do
      {
        Console.WriteLine(menuText);
        keyInput = Console.ReadKey(true);
        choiceMenuItem = choiceVariants.Contains((int) keyInput.Key);
      } 
      while (!choiceMenuItem);

      return (T)(object)keyInput.Key;
    }

    public static void Clear()
    {
      Console.Clear();
    }
  }
}
