﻿using CompileDemo.InteractionArea.Attributes;

namespace CompileDemo.InteractionArea
{
  [InputHeader("Настройка генерации данных:")]
  internal class InputDataGenerate
  {
    [InputQuestion("Файлов в секунду")]
    public uint FilesPerSecond;

    [InputQuestion("Размер массива в файле")]
    public uint ArrayLength;
  }
}
