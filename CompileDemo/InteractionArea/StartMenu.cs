﻿using System;
using CompileDemo.InteractionArea.Attributes;

namespace CompileDemo.InteractionArea
{
  internal class StartMenu
  {
    [HeaderDescription("Выберите действие:")]
    public enum Variants
    {
      [VariantDescription("Создать рабочие директории")]
      CreateDirectories = ConsoleKey.C,

      [VariantDescription("Очистить рабочие директории")]
      CleanDirectories = ConsoleKey.D,

      [VariantDescription("Слудующий шаг")]
      Next = ConsoleKey.N,

      [VariantDescription("Выход")]
      Exit = ConsoleKey.Q
    }
  }
}
