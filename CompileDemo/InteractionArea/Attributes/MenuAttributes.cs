﻿using System;
using System.ComponentModel;

namespace CompileDemo.InteractionArea.Attributes
{
  [AttributeUsage(AttributeTargets.Enum, AllowMultiple =false)]
  internal class HeaderDescriptionAttribute : DescriptionAttribute
  {
    internal HeaderDescriptionAttribute(string text) : base(text) { }
  }

  [AttributeUsage(AttributeTargets.Field, AllowMultiple =false)]
  internal class VariantDescriptionAttribute : DescriptionAttribute
  {
    internal VariantDescriptionAttribute(string text) : base(text) { }
  }
}
