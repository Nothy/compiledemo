﻿using System;
using System.Linq;
using System.Text;

namespace CompileDemo.InteractionArea.Attributes
{
  internal class MenuAttributesReader<T>
  {
    public string Read()
    {
      var result = new StringBuilder();

      result.AppendLine(VariantHeaderRead());

      var variantItems = Enum.GetValues(typeof(T));

      foreach (Enum variantItem in variantItems)
      {
        var variantDescription = VariantDescriptionRead(variantItem);
        result.AppendLine(variantDescription);
      }

      return result.ToString();
    }

    private string VariantHeaderRead()
    {
      var attributes = (HeaderDescriptionAttribute[])typeof(T).GetCustomAttributes(typeof(HeaderDescriptionAttribute), false);

      if (!attributes.Any())
        return string.Empty;

      return attributes[0].Description;
    }

    private string VariantDescriptionRead(Enum variantItem)
    {
      var field = variantItem.GetType().GetField(variantItem.ToString());
      var attributes = (VariantDescriptionAttribute[])field.GetCustomAttributes(typeof(VariantDescriptionAttribute), false);

      var keySymbol = ((ConsoleKey)variantItem).ToString();

      if (attributes.Length > 0)
        return $"{keySymbol} - {attributes[0].Description}";
      else
        return $"{keySymbol}";
    }
  }
}
