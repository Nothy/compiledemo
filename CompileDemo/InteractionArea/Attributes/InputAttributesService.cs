﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CompileDemo.InteractionArea.Attributes
{
  internal class InputAttributesService<T> where T : class, new()
  {
    public string HeaderRead()
    {
      var headerAttribute = typeof(T).GetCustomAttributes().OfType<InputHeaderAttribute>().FirstOrDefault();

      if (headerAttribute == null)
        return string.Empty;

      return headerAttribute.Description;
    }

    public IEnumerable<InputData> QuestionsRead()
    {
      var result = new List<InputData>();

      foreach (var fieldInfo in typeof(T).GetFields())
      {
        var questionAttribute =
        fieldInfo.GetCustomAttributes().OfType<InputQuestionAttribute>().FirstOrDefault();

        result.Add(new InputData
        {
          Question = questionAttribute.Description,
          FieldName = fieldInfo.Name,
        });

      }

      return result;
    }

    public bool AnswerTypeValidate(string fieldName, string answer)
    {
      var fieldInfo = typeof(T).GetField(fieldName);
      var type = fieldInfo.FieldType;

      try
      {
        var tAnsware = Convert.ChangeType(answer, type);
        return tAnsware != null;
      }
      catch
      {
        return false;
      }
    }

    public T BuildResult(IEnumerable<InputData> inputData)
    {
      var result = new T();

      foreach (var data in inputData)
      {
        var fieldInfo = typeof(T).GetField(data.FieldName);
        var tAnswer = Convert.ChangeType(data.Answer, fieldInfo.FieldType);
        fieldInfo.SetValue(result, tAnswer);
      }

      return result;
    }

    public class InputData
    {
      public string Question;
      public string FieldName;
      public string Answer;
    }
  }
}
