﻿using System;
using System.ComponentModel;

namespace CompileDemo.InteractionArea.Attributes
{
  [AttributeUsage(AttributeTargets.Class, AllowMultiple =false)]
  internal class InputHeaderAttribute : DescriptionAttribute
  {
    internal InputHeaderAttribute(string text) : base(text) { }
  }

  [AttributeUsage(AttributeTargets.Field, AllowMultiple =false)]
  internal class InputQuestionAttribute : DescriptionAttribute
  {
    internal InputQuestionAttribute(string text) : base(text) { }
  }
}
