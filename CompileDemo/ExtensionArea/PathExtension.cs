﻿namespace CompileDemo.ExtensionArea
{
  internal static class PathExt
  {
    public static string GetExtensionWithoutPoint(string fileName)
    {
      var result = System.IO.Path.GetExtension(fileName).Replace(".", string.Empty);
      return result;
    }
  }
}
