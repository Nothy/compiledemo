﻿using System.Collections.Generic;

namespace CompileDemo.ExtensionArea
{
  internal static class HashSetExtension
  {
    public static void RemoveRange<T>(this HashSet<T> list, List<T> removeItems)
    {
      foreach (var removeItem in removeItems)
        list.Remove(removeItem);
    }

    public static void RemoveRange<T>(this HashSet<T> list, IEnumerable<T> removeItems)
    {
      foreach (var removeItem in removeItems)
        list.Remove(removeItem);
    }

    public static void AddRange<T>(this HashSet<T> list, IEnumerable<T> removeItems)
    {
      foreach (var removeItem in removeItems)
        list.Add(removeItem);
    }
  }
}
