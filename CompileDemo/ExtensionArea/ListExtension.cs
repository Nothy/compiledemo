﻿using System.Collections.Generic;

namespace CompileDemo.ExtensionArea
{
  internal static class ListExtension
  {
    public static void RemoveRange<T>(this List<T> list, List<T> removeItems)
    {
      foreach (var removeItem in removeItems)
        list.Remove(removeItem);
    }

    public static void RemoveRange<T>(this List<T> list, IEnumerable<T> removeItems)
    {
      foreach (var removeItem in removeItems)
        list.Remove(removeItem);
    }
  }
}
