﻿// ReSharper disable CheckNamespace
namespace CompileDemo.DemoCs.MultiplyingByThree
{
  public class DataConverter
  {
    public byte[] Convert(byte[] In)
    {
      var result = new byte[In.Length];
      for (int item = 0; item < In.Length; item++)
      {
        result[item] = (byte) (In[item] * 3);
      }

      return result;
    }
  }
}