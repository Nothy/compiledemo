﻿// ReSharper disable CheckNamespace
namespace CompileDemo.DemoCs.AddByTwo
{
  public class DataConverter
  {
    public byte[] Convert(byte[] In)
    {
      var result = new byte[In.Length];
      for (int item = 0; item < In.Length; item++)
      {
        result[item] = (byte)(In[item] + 2);
      }

      return result;
    }
  }
}