﻿using System;
using System.IO;
using CompileDemo.InteractionArea;
using CompileDemo.SettingsArea;

namespace CompileDemo.Services
{
  internal static class MainDirectoryService
  {
    public static void Create()
    {
      foreach (var directory in AppSettings.Directories())
      {
        var targetDirectory = new DirectoryInfo(directory);

        if (!targetDirectory.Exists)
        {
          targetDirectory.Create();
          Interaction.WriteSuccessMessage($"Директория {directory} создана");
        }
        else
        {
          Interaction.WriteMessage($"Директория {directory} уже существует");
        }

      }
    }

    public static void CleanAll()
    {
      Console.Clear();

      foreach (var directory in AppSettings.Directories())
      {
        var targetDirectory = new DirectoryInfo(directory);

        if (!targetDirectory.Exists)
          continue;

        foreach (var file in targetDirectory.GetFiles())
          file.Delete();

        Interaction.WriteSuccessMessage($"Директория {directory} очищена");
      }
    }

    public static void CleanDllDirectory()
    {
      var targetDirectory = new DirectoryInfo(AppSettings.DllDir);

      foreach (var file in targetDirectory.GetFiles())
        file.Delete();
      Interaction.WriteSuccessMessage($"Директория {AppSettings.DllDir} очищена");
    }

    public static void CleanTemporaryDirectory()
    {
      var targetDirectory = new DirectoryInfo(AppSettings.TemporaryDir);

      foreach (var file in targetDirectory.GetFiles())
        file.Delete();
      Interaction.WriteSuccessMessage($"Директория {AppSettings.TemporaryDir} очищена");
    }
  }
}
