﻿using System;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using CompileDemo.ExtensionArea;
using CompileDemo.Services.FileObserverArea;
using CompileDemo.SettingsArea;

namespace CompileDemo.Services
{
  internal static class DllLoader
  {
    static DllLoader()
    {
      dllTypes = new ConcurrentBag<DllItem>();
    }

    private static readonly ConcurrentBag<DllItem> dllTypes;

    public static ReadOnlyCollection<DllItem> DllTypes => new ReadOnlyCollection<DllItem>(dllTypes.ToArray());

    public static void Start()
    {
      FileObserver.DllAppeared += LoadHandler;
      FileObserver.PreCompiledScriptDelete += UnLoad;
    }

    public static void Stop()
    {
      FileObserver.DllAppeared -= LoadHandler;
      FileObserver.PreCompiledScriptDelete -= UnLoad;
    }

    private static void LoadHandler(string dllFileName)
    {
      var fileDll = new FileInfo(dllFileName);
      Load(fileDll);
    }

    private static void UnLoad(string scriptName)
    {
      var fileIndexName = Path.GetFileNameWithoutExtension(scriptName);

      var unloadDllItem = dllTypes
        .Where(d => d.IsActive && d.FileIndexName.Equals(fileIndexName, StringComparison.InvariantCultureIgnoreCase));

      if (!unloadDllItem.Any())
        return;

      foreach (var dll in unloadDllItem)
        dll.IsActive = false;
    }

    private static void Load(FileInfo dllFile)
    {
      UnLoad(Path.GetFileNameWithoutExtension(dllFile.Name));

      var dll = Assembly.LoadFile(dllFile.FullName);

      var classType = dll.GetTypes().FirstOrDefault(t => string.Equals(t.Name, AppSettings.ScriptClassName, StringComparison.OrdinalIgnoreCase));

      if (classType == null)
        throw new ApplicationException($"Файл скрипта {dllFile.Name} не имеет тип {AppSettings.ScriptClassName}");

      TryUse(classType);

      var dllItem = new DllItem(dllFile, classType);

      dllTypes.Add(dllItem);
    }

    private static void TryUse(Type typeUnderTest)
    {
      var classInstance = Activator.CreateInstance(typeUnderTest);
      var methodDelegate = (Func<byte[], byte[]>)Delegate.CreateDelegate(typeof(Func<byte[], byte[]>), classInstance, AppSettings.ScriptMethodName);
      methodDelegate.Invoke(AppSettings.TEST_BYTE_ARRAY);
    }
  }

  internal class DllItem
  {
    public DllItem(FileInfo file, Type usingType, bool isActive = true)
    {
      File = file;

      var fileName = Path.GetFileNameWithoutExtension(file.Name);
      FileIndexName = Path.GetFileNameWithoutExtension(fileName);
      FileNumber = int.Parse(PathExt.GetExtensionWithoutPoint(fileName));
      UsingType = usingType;
      IsActive = isActive;
    }

    public bool IsActive;
    public readonly int FileNumber;
    public readonly string FileIndexName;
    public readonly FileInfo File;
    public readonly Type UsingType;
  }
}
