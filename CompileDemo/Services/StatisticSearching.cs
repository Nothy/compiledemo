﻿using System.Linq;
using System.Text;
using CompileDemo.Services.FileObserverArea;
using CompileDemo.Services.ProcessorArea;

namespace CompileDemo.Services
{
  internal static class StatisticSearching
  {
    static StatisticSearching()
    {
      dllCount = 0;
      preProcessedDataRegisteredCount = 0;

    }

    private static int dllCount;
    private static int preProcessedDataRegisteredCount;
    private static int processedDataCount;

    public static void Start()
    {
      FileObserver.DllAppeared += DllAppeared;
      FileObserver.PreProcessedDataAppeared += PreProcessedDataAppeared;
      Processor.FileProcessedEvent += FileProcessedEvent;
    }

    public static void Stop()
    {
      FileObserver.DllAppeared -= DllAppeared;
      FileObserver.PreProcessedDataAppeared -= PreProcessedDataAppeared;
      Processor.FileProcessedEvent -= FileProcessedEvent;
    }

    private static void DllAppeared(string s)
    {
      dllCount++;
    }

    private static void PreProcessedDataAppeared(string s)
    {
      preProcessedDataRegisteredCount++;
    }

    private static void FileProcessedEvent(string s)
    {
      processedDataCount++;
    }

    public static string Search()
    {
      var preResult = new StringBuilder();
      var dllInUse = DllLoader.DllTypes.Count(t => t.IsActive);

      preResult.AppendLine($"Файлов данных зарегистрированно: {preProcessedDataRegisteredCount}, обработано: {processedDataCount}");
      preResult.AppendLine($"Файлов скриптов скомпилировано: {dllCount}, применяется: {dllInUse}");

      var result = preResult.ToString();
      return result;
    }
  }
}
