﻿using System;
using System.CodeDom.Compiler;
using System.IO;
using CompileDemo.Services.FileObserverArea;
using CompileDemo.SettingsArea;

namespace CompileDemo.Services
{
  public static class Compilator
  {
    public static void Start()
    {
      FileObserver.PreCompiledScriptAppeared += ScriptAppearedHandler;
    }

    public static void Stop()
    {
      FileObserver.PreCompiledScriptAppeared -= ScriptAppearedHandler;
    }

    private static void ScriptAppearedHandler(string fileName)
    {
      Compile(new FileInfo(fileName));
    }

    private static void Compile(FileInfo scriptFile)
    {
      var csText = File.ReadAllText(scriptFile.FullName);
      var resultFilePath = GetResultFilePath(scriptFile);

      var provider = CodeDomProvider.CreateProvider("CSharp");
      var parameters = new CompilerParameters();

      parameters.GenerateInMemory = false;
      parameters.GenerateExecutable = false;
      parameters.IncludeDebugInformation = false;
      parameters.OutputAssembly = resultFilePath;
      parameters.CompilerOptions = "/optimize";
      parameters.TempFiles = new TempFileCollection(AppSettings.TemporaryDir, true);

      var compiler = provider.CreateCompiler();
      var compileResult = compiler.CompileAssemblyFromSource(parameters, csText);

      if (compileResult.Errors.Count != 0)
        throw new Exception(compileResult.Errors[0].ToString());
    }

    private static string GetResultFilePath(FileSystemInfo scriptFile)
    {
      var fileFullNameWithoutExtension = $"{Path.Combine(AppSettings.DllDir, Path.GetFileNameWithoutExtension(scriptFile.Name))}";

      var dublicateFileNum = 1;
      var file = new FileInfo($"{fileFullNameWithoutExtension}.{dublicateFileNum}.{AppSettings.DLL_FILE_EXTENSION}");
      
      while (file.Exists)
      {
        file = new FileInfo($"{fileFullNameWithoutExtension}.{dublicateFileNum}.{AppSettings.DLL_FILE_EXTENSION}");
        dublicateFileNum++;
      }

      return file.FullName;
    }
  }
}
