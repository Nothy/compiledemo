﻿using System;
using System.IO;
using CompileDemo.SettingsArea;

namespace CompileDemo.Services
{
  using SystemTimer = System.Timers.Timer;

  internal static class DataGenerator
  {
    static DataGenerator()
    {
      timer = new SystemTimer();
      timer.Interval = 1000;
      timer.AutoReset = true;
      timer.Elapsed += TimerTick;

      rnd = new Random();
    }

    private static readonly SystemTimer timer;
    private static readonly Random rnd;

    public static void Start()
    {
      timer.Enabled = true;
    }

    public static void Stop()
    {
      timer.Enabled = false;
    }

    private static void TimerTick(object sender, System.Timers.ElapsedEventArgs e)
    {
      var senderTimer = sender as SystemTimer;
      senderTimer.Elapsed -= TimerTick;

      for (var fileCounter = 0; fileCounter < DataGenerateSettings.FilesPerSecond; fileCounter++)
      {
        var filePath = GetPath();
        var data = GetData();
        CreateFile(filePath, data);
      }

      senderTimer.Elapsed += TimerTick;
    }

    private static void CreateFile(string filePath, byte[] data)
    {
      var line = string.Join(",", data);
      File.WriteAllText(filePath, line);
    }

    private static string GetPath()
    {
      var newFileName = $"{ Guid.NewGuid():N}.{AppSettings.DATA_FILE_EXTENSION}";
      var newFilePath = Path.Combine(AppSettings.PreProcessedDataDir, newFileName);
      return newFilePath;
    }

    private static byte[] GetData()
    {
      var numbers = new byte[DataGenerateSettings.ArrayLength];
      rnd.NextBytes(numbers);
      return numbers;
    }
  }
}
