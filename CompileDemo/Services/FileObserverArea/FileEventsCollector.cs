﻿using System;
using System.IO;
using System.Timers;

namespace CompileDemo.Services.FileObserverArea
{
  internal static class FileEventsCollector
  {
    static FileEventsCollector()
    {
      processingEventsPack = new EventsPack();
      accumulationEventsPack = new EventsPack();

      timer = new Timer();
      timer.Interval = 1000;
      timer.AutoReset = true;
      timer.Elapsed += TimerTick;
    }

    private static readonly Timer timer;
    private static EventsPack processingEventsPack;
    private static EventsPack accumulationEventsPack;

    public static void Start()
    {
      timer.Enabled = true;
    }

    public static void Stop()
    {
      timer.Enabled = false;
    }

    private static void TimerTick(object sender, ElapsedEventArgs e)
    {
      var senderTimer = sender as Timer;
      senderTimer.Elapsed -= TimerTick;

      processingEventsPack.RemoveMatchings(accumulationEventsPack);
      var blockedFileItems = processingEventsPack.Extract(i => !IsFileFree(i.Item2));
      processingEventsPack.Do();
      accumulationEventsPack.AddEvents(blockedFileItems);
      processingEventsPack = accumulationEventsPack;

      accumulationEventsPack = new EventsPack();

      senderTimer.Elapsed += TimerTick;
    }

    public static void AddEvent(Action<string> @event, string argument)
    {
      accumulationEventsPack.AddEvent(@event, argument);
    }

    private static bool IsFileFree(string fileFullPath)
    {
      var result = true;
      try
      {
        var fs = File.Open(fileFullPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
        fs.Close();
      }
      catch
      {
        result = false;
      }
      return result;
    }
  }
}
