﻿using System;
using System.IO;
using CompileDemo.SettingsArea;

namespace CompileDemo.Services.FileObserverArea
{
  internal static class FileObserver
  {
    static FileObserver()
    {
      preCompiledScriptWatcher = new FileSystemWatcher();
      preCompiledScriptWatcher.EnableRaisingEvents = false;
      preCompiledScriptWatcher.Path = AppSettings.PreCompiledScriptDir;
      preCompiledScriptWatcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName;
      preCompiledScriptWatcher.Filter = $"*.{AppSettings.SCRIPT_FILE_EXTENSION}";
      preCompiledScriptWatcher.Created += PreCompiledScriptCreated;
      preCompiledScriptWatcher.Changed += PreCompiledScriptCreated;
      preCompiledScriptWatcher.Deleted += PreCompiledScriptDeleted;
      preCompiledScriptWatcher.Renamed += PreCompiledScriptRenamed;

      preProcessedDataWatcher = new FileSystemWatcher();
      preProcessedDataWatcher.EnableRaisingEvents = false;
      preProcessedDataWatcher.Path = AppSettings.PreProcessedDataDir;
      preProcessedDataWatcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName;
      preProcessedDataWatcher.Filter = $"*.{AppSettings.DATA_FILE_EXTENSION}";
      preProcessedDataWatcher.Changed += PreProcessedDataCreated;
      preProcessedDataWatcher.Created += PreProcessedDataCreated;

      dllWatcher = new FileSystemWatcher();
      dllWatcher.EnableRaisingEvents = false;
      dllWatcher.Path = AppSettings.DllDir;
      dllWatcher.NotifyFilter = NotifyFilters.LastWrite;
      dllWatcher.Filter = $"*.{AppSettings.DLL_FILE_EXTENSION}";
      dllWatcher.Changed += DllCreated;
    }

    private static readonly FileSystemWatcher preProcessedDataWatcher;
    private static readonly FileSystemWatcher preCompiledScriptWatcher;
    private static readonly FileSystemWatcher dllWatcher;

    public static Action<string> PreProcessedDataAppeared;
    public static Action<string> PreCompiledScriptAppeared;
    public static Action<string> DllAppeared;
    public static Action<string> PreCompiledScriptDelete;

    public static void Start()
    {
      FileEventsCollector.Start();

      preProcessedDataWatcher.EnableRaisingEvents = true;
      preCompiledScriptWatcher.EnableRaisingEvents = true;
      dllWatcher.EnableRaisingEvents = true;
      CollectExistsFiles();
    }

    public static void Stop()
    {
      FileEventsCollector.Stop();

      preProcessedDataWatcher.EnableRaisingEvents = false;
      preCompiledScriptWatcher.EnableRaisingEvents = false;
      dllWatcher.EnableRaisingEvents = false;
    }

    private static void DllCreated(object sender, FileSystemEventArgs e)
    {
      FileEventsCollector.AddEvent(DllAppeared, e.FullPath);
    }

    private static void PreProcessedDataCreated(object sender, FileSystemEventArgs e)
    {
      FileEventsCollector.AddEvent(PreProcessedDataAppeared, e.FullPath);
    }

    private static void PreCompiledScriptDeleted(object sender, FileSystemEventArgs e)
    {
      PreCompiledScriptDelete?.Invoke(e.FullPath);
    }

    private static void PreCompiledScriptCreated(object sender, FileSystemEventArgs e)
    {
      FileEventsCollector.AddEvent(PreCompiledScriptAppeared, e.FullPath);
    }

    private static void PreCompiledScriptRenamed(object sender, RenamedEventArgs e)
    {
      FileEventsCollector.AddEvent(PreCompiledScriptDelete, e.OldName);
      FileEventsCollector.AddEvent(PreCompiledScriptAppeared, e.FullPath);
    }

    private static void CollectExistsFiles()
    {
      var lastStateDataWatcher = preProcessedDataWatcher.EnableRaisingEvents;
      preProcessedDataWatcher.EnableRaisingEvents = false;
      var dataSourceDirectory = new DirectoryInfo(AppSettings.PreProcessedDataDir);
      var dataSourceFiles = dataSourceDirectory.GetFiles($"*.{AppSettings.DATA_FILE_EXTENSION}");
      foreach (var fileName in dataSourceFiles)
        PreProcessedDataCreated(null, new FileSystemEventArgs(WatcherChangeTypes.All, AppSettings.PreProcessedDataDir, fileName.Name));
      preProcessedDataWatcher.EnableRaisingEvents = lastStateDataWatcher;

      var lastStateScriptWatcher = preCompiledScriptWatcher.EnableRaisingEvents;
      preCompiledScriptWatcher.EnableRaisingEvents = false;
      var scriptSourceDirectory = new DirectoryInfo(AppSettings.PreCompiledScriptDir);
      var scriptSourceFiles = scriptSourceDirectory.GetFiles($"*.{AppSettings.SCRIPT_FILE_EXTENSION}");
      foreach (var fileName in scriptSourceFiles)
        PreCompiledScriptCreated(null, new FileSystemEventArgs(WatcherChangeTypes.All, AppSettings.PreCompiledScriptDir, fileName.Name));
      preCompiledScriptWatcher.EnableRaisingEvents = lastStateScriptWatcher;
    }
  }
}
