﻿using System;
using System.Collections.Generic;
using System.Linq;
using CompileDemo.ExtensionArea;

namespace CompileDemo.Services.FileObserverArea
{
  using ArgsAction = Tuple<Action<string>, string>;

  internal class EventsPack
  {
    private readonly HashSet<ArgsAction> events = new HashSet<ArgsAction>();

    public void AddEvent(Action<string> @event, string argument)
    {
      events.Add(Tuple.Create(@event, argument));
    }

    public void AddEvents(IEnumerable<ArgsAction> addEvents)
    {
      events.AddRange(addEvents);
    }

    public void RemoveMatchings(EventsPack eventsPack)
    {
      var removeEvents = events.Where(e => eventsPack.events.Contains(e)).ToList();
      events.RemoveRange(removeEvents);
    }

    public IEnumerable<Tuple<Action<string>, string>> Extract(Func<ArgsAction, bool> condition)
    {
      var removeItems = events.Where(condition).ToList();

      events.RemoveRange(removeItems);

      return removeItems;
    }

    public void Do()
    {
      foreach (var @event in events)
        @event.Item1.Invoke(@event.Item2);
    }
  }
}
