﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace CompileDemo.Services.ProcessorArea
{
  internal class Pool<T>
  {
    private readonly ConcurrentBag<T> objects;
    private readonly Func<T> tGenerator;
    private readonly int maxItemsCount;
    private int usingCount;

    public Pool(Func<T> tGenerator, int maxItemsCount)
    {
      if (tGenerator == null)
        throw new ArgumentNullException("objectGenerator");

      if (maxItemsCount < 1)
        throw new ArgumentException("maxCount");

      this.maxItemsCount = maxItemsCount;
      this.tGenerator = tGenerator;

      usingCount = 0;
      objects = new ConcurrentBag<T>();
    }

    public bool TryGetObject(out T value)
    {
      value = default(T);
      if (usingCount >= maxItemsCount)
        return false;

      if (objects.TryTake(out var item))
      {
        value = item;
      }
      else
      {
        value = tGenerator();
        usingCount++;
      }

      return true;
    }

    public void PutObject(T item)
    {
      objects.Add(item);
      Interlocked.Decrement(ref usingCount);
    }
  }
}
