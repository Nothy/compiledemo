﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using CompileDemo.Services.FileObserverArea;

namespace CompileDemo.Services.ProcessorArea
{
  internal static class Processor
  {
    static Processor()
    {
      pipelinePool = new Pool<DataProcessingPipeline>(DataProcessingPipeline.Create, Environment.ProcessorCount);
      preProcessedDataFiles = new ConcurrentQueue<string>();
      loopToken = new CancellationTokenSource();
    }

    private static readonly Pool<DataProcessingPipeline> pipelinePool;
    private static ConcurrentQueue<string> preProcessedDataFiles;
    private static CancellationTokenSource loopToken;

    public static Action<string> FileProcessedEvent;

    public static void Start()
    {
      if (loopToken != null)
      {
        if (loopToken.Token.CanBeCanceled)
          loopToken.Cancel();
        loopToken.Dispose();
      }

      loopToken = new CancellationTokenSource();

      var loop = new Task(ConveyorLoop, loopToken.Token);
      loop.Start();

      FileObserver.PreProcessedDataAppeared += PreProcessedDataAppeared;
    }

    public static void Stop()
    {
      FileObserver.PreProcessedDataAppeared -= PreProcessedDataAppeared;
      loopToken.Cancel();
      preProcessedDataFiles = new ConcurrentQueue<string>();
    }

    private static void PreProcessedDataAppeared(string dataFilePath)
    {
      preProcessedDataFiles.Enqueue(dataFilePath);
    }

    private static void ConveyorLoop(object tokenObj)
    {
      var token = (CancellationToken)tokenObj;

      while (!token.IsCancellationRequested)
      {

        string preprocessedDataFilePath;
        DataProcessingPipeline pipeline;

        while (!preProcessedDataFiles.TryDequeue(out preprocessedDataFilePath))
          Task.Delay(1000).Wait();

        while (!pipelinePool.TryGetObject(out pipeline))
          Task.Delay(1000).Wait();

        try
        {
          var fileProcessTask = new Task(() =>
          {
            pipeline.ProcessDataFile(preprocessedDataFilePath);
            FileProcessedEvent(preprocessedDataFilePath);

            pipeline.Update();
            pipelinePool.PutObject(pipeline);
          }, TaskCreationOptions.AttachedToParent);

          fileProcessTask.Start();
        }
        catch (Exception ex)
        {
          if (preprocessedDataFilePath != string.Empty)
            preProcessedDataFiles.Enqueue(preprocessedDataFilePath);
        }
      }
    }
  }


}
