﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CompileDemo.ExtensionArea;
using CompileDemo.SettingsArea;

namespace CompileDemo.Services.ProcessorArea
{
  // FileIndexName, FileNumber, Function
  using PipelineItem = Tuple<string, int, Func<byte[], byte[]>>;

  internal class DataProcessingPipeline
  {
    public DataProcessingPipeline()
    {
      pipeline = new List<PipelineItem>();
    }

    private List<PipelineItem> pipeline;

    public byte[] Processing(byte[] data)
    {
      var result = pipeline.Select(p => p.Item3).Aggregate(data, (resultFunction, currentFunction) => currentFunction(resultFunction));
      return result;
    }

    public void ProcessDataFile(string preprocessedDataFilePath)
    {
      var fileData = File.ReadAllText(preprocessedDataFilePath);

      var array = fileData.Split(new[] { ',' }, int.MaxValue, StringSplitOptions.RemoveEmptyEntries)
        .Select(i =>
        {
          var hasValue = byte.TryParse(i, out var parseResult);
          return new { hasValue, parseResult };
        })
        .Where(i => i.hasValue)
        .Select(i => i.parseResult)
        .ToArray();

      var processResult = Processing(array);

      var resultFileName = GetResultFilePath(preprocessedDataFilePath);

      var line = string.Join(",", processResult);

      File.WriteAllText(resultFileName, line);

      File.Delete(preprocessedDataFilePath);
    }

    private static string GetResultFilePath(string dataFile)
    {
      var newFileName = $"{ Path.GetFileName(dataFile)}";
      var newFilePath = Path.Combine(AppSettings.ProcessedDataDir, newFileName);
      return newFilePath;
    }

    public static DataProcessingPipeline Create()
    {
      var result = new DataProcessingPipeline();

      result.pipeline = DllLoader.DllTypes
        .Where(d => d.IsActive)
        .OrderBy(d => d.FileIndexName)
        .Select(
        d =>
        {
          var classInstance = Activator.CreateInstance(d.UsingType);
          var @delegate = (Func<byte[], byte[]>)Delegate.CreateDelegate(
            typeof(Func<byte[], byte[]>), classInstance, AppSettings.ScriptMethodName);

          var pipelineItem = Tuple.Create(d.FileIndexName, d.FileNumber, @delegate);

          return pipelineItem;
        }
      ).ToList();

      return result;
    }

    public void Update()
    {
      var activeTypes = DllLoader.DllTypes.Where(d => d.IsActive).ToList();

      var removeTypes = pipeline.Where(p => !activeTypes
        .Any(t => t.FileNumber == p.Item2 && string.Equals(t.FileIndexName, p.Item1, StringComparison.OrdinalIgnoreCase)))
        .ToList();

      var addTypes = activeTypes.Where(t => !pipeline
        .Any(p => t.FileNumber == p.Item2 && string.Equals(t.FileIndexName, p.Item1, StringComparison.OrdinalIgnoreCase)))
        .ToList();

      pipeline.RemoveRange(removeTypes);

      pipeline.AddRange(
        addTypes.Select(
          d =>
          {
            var classInstance = Activator.CreateInstance(d.UsingType);
            var @delegate = (Func<byte[], byte[]>)Delegate.CreateDelegate(
              typeof(Func<byte[], byte[]>), classInstance, AppSettings.ScriptMethodName);

            var pipelineItem = Tuple.Create(d.FileIndexName, d.FileNumber, @delegate);

            return pipelineItem;
          }));

      pipeline.Sort((p1, p2) => string.CompareOrdinal(p1.Item1, p2.Item1));
    }
  }
}
